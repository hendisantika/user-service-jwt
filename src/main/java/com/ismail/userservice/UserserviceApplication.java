package com.ismail.userservice;

import com.ismail.userservice.domain.Role;
import com.ismail.userservice.domain.User;
import com.ismail.userservice.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class UserserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserserviceApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveRole(new Role(null, "ROLE_USER"));
            userService.saveRole(new Role(null, "ROLE_MANAGER"));
            userService.saveRole(new Role(null, "ROLE_ADMIN"));
            userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

            userService.saveUser(new User(null, "Ismail Fajar", "ismail", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Hendi Santika", "hendi", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Kakashi Hatake", "kakashi", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Siti Yuliatin", "siti", "1234", new ArrayList<>()));

            userService.addRoleToUser("ismail", "ROLE_USER");
            userService.addRoleToUser("ismail", "ROLE_MANAGER");
            userService.addRoleToUser("hendi", "ROLE_MANAGER");
            userService.addRoleToUser("kakashi", "ROLE_USER");
            userService.addRoleToUser("siti", "ROLE_USER");
            userService.addRoleToUser("siti", "ROLE_MANAGER");
            userService.addRoleToUser("siti", "ROLE_SUPER_ADMIN");

        };
    }
}
